//
//  Element.swift
//  ShowRedditProyect
//
//  Created by User on 3/24/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class Element: AnyObject
{
    let title:String
    let author:String
    let dateIn:Date
    let thumbnailURL:String
    let countComments:Int
    let subreddit:String
    
    init(title:String,author:String,dateIn:Date,thumbnailURL:String,countComments:Int,subreddit:String)
    {
        self.title = title
        self.author = author
        self.dateIn = dateIn
        self.thumbnailURL = thumbnailURL
        self.countComments = countComments
        self.subreddit = subreddit
    }
}
