//
//  InfoTableViewCell.swift
//  ShowRedditProyect
//
//  Created by User on 3/24/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var infoImage: DownloadImageView!
    @IBOutlet weak var infoTitle: UILabel!
    @IBOutlet weak var detailLabel_1: UILabel!
    @IBOutlet weak var detailLabel_2: UILabel!
    @IBOutlet weak var detailLabel_3: UILabel!
    @IBOutlet weak var detailLabel_4: UILabel!
    
    @IBOutlet weak var heightInfoTitle: NSLayoutConstraint!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
