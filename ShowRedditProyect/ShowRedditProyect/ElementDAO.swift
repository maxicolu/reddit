//
//  ElementDAO.swift
//  ShowRedditProyect
//
//  Created by User on 3/25/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit
import CoreData

class ElementDAO: AnyObject
{
    private var elements:Array<Element>?
    
    func getElements() -> Array<Element>
    {
        guard elements == nil else {return elements!}
        
         elements = Array<Element>()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName:"Element")
        request.returnsObjectsAsFaults = false
         
        do
        {
            let results = try context.fetch(request)
            if results.count > 0
            {
                for entity in results as! [NSManagedObject]
                {
                    let title = entity.value(forKey:"title" ) as! String
                    let author = entity.value(forKey:"author" ) as! String
                    let dateIn = entity.value(forKey:"dateIn" ) as! Date
                    let thumbnailURL = entity.value(forKey:"thumbnailURL" ) as! String
                    let countComments = entity.value(forKey:"countComments" ) as! Int
                    let subreddit = entity.value(forKey:"subreddit" ) as! String
                        
                    let element = Element(title: title, author: author, dateIn:dateIn, thumbnailURL: thumbnailURL, countComments: countComments,subreddit:subreddit)
                    elements!.append(element)
                }
            }
        }
        catch
        {
        }
        return elements!
    }
    
    func cleanAllElements() -> ()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Element")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        }
        catch let error as NSError {
            // Handle error
            print(error.localizedDescription)
        }
        
        elements?.removeAll()
    }
    
    func assignElements(elementsToAppend:Array<Element>) -> ()
    {
        self.cleanAllElements()
        
        if elements == nil
        {
            elements = Array<Element>()
        }
        elements!.removeAll()
        elements!.append(contentsOf: elementsToAppend)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        for element in elements!
        {
            let newElement = NSEntityDescription.insertNewObject(forEntityName: "Element", into: context)
            
            newElement.setValue(element.title, forKey: "title")
            newElement.setValue(element.author, forKey: "author")
            newElement.setValue(element.dateIn, forKey: "dateIn")
            newElement.setValue(element.thumbnailURL, forKey: "thumbnailURL")
            newElement.setValue(element.countComments, forKey: "countComments")
            newElement.setValue(element.subreddit, forKey: "subreddit")
        }
        
        do
        {
            try context.save()
        }
        catch
        {
        }
        
    }
}
