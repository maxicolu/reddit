//
//  HTTPClient.swift
//  ShowRedditProyect
//
//  Created by User on 3/23/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class HTTPClient: AnyObject
{
    static let sharedInstance = HTTPClient()
    
    private let URLService = "https://www.reddit.com/top/.json"
    
    func getData(completion:@escaping (Data?,Error?) -> ()) -> ()
    {
        let request = URLRequest(url:URL(string: URLService)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            
            if (data != nil)
            {
                completion(data,error)
            }
            else
            {
                completion(nil,error)
            }
        }
        
        task.resume()
    }
    
    func getDataImage(URLImage:String,completion:@escaping (Data?,Error?) -> ()) -> ()
    {
        let request = URLRequest(url:URL(string: URLImage)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            
            if (data != nil)
            {
                DispatchQueue.main.async
                {
                    completion(data,error)
                }
                
            }
            
        }
        task.resume()
    }
}
