//
//  LibraryAPI.swift
//  ShowRedditProyect
//
//  Created by User on 3/23/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class LibraryAPI: AnyObject
{
    static let sharedInstance = LibraryAPI()
    let internetReachability = Reachability.forInternetConnection()
    
    func getElements(completion:@escaping ((Array<Element>?,Error?) -> ()) ) -> ()
    {
        let internetStatus = internetReachability?.currentReachabilityStatus()
        
        guard internetStatus != NotReachable else
        {
            completion(ElementDAO().getElements(),nil)
            return
        }
        
        HTTPClient.sharedInstance.getData { (response:Data?, error:Error?) in
            
            if error != nil
            {
                completion(nil,error)
            }
            else
            {
                var elementsArray = Array<Element>()
                do
                {
                    let JSONObjArray = try JSONSerialization.jsonObject(with: response!, options: JSONSerialization.ReadingOptions.allowFragments)
                    let data = ((JSONObjArray as? Dictionary<String,AnyObject>)?["data"])
                    let childrenArray = data?["children"] as? Array<Dictionary<String,AnyObject>>
                    
                    if childrenArray != nil
                    {
                        for child in childrenArray!
                        {
                            let childData = child["data"] as? Dictionary<String,AnyObject>
                            if childData != nil
                            {
                                let title = childData!["title"] as! String
                                let author = childData!["author"] as! String
                                let dateIn = childData!["created_utc"] as! Float
                                let thumbnailURL = childData!["thumbnail"] as! String
                                let countComments = childData!["num_comments"] as! Int
                                let subreddit = childData!["subreddit"] as! String
                                
                                let dateCreated = Date(timeIntervalSince1970: TimeInterval(dateIn))
                                let element = Element(title: title, author: author, dateIn:dateCreated, thumbnailURL: thumbnailURL, countComments: countComments,subreddit:subreddit)
                                elementsArray.append(element)
                            }
                            //print(childData?["title"])
                        }
                    }
                    else
                    {
                        completion(nil,error)
                    }
                }
                catch{}
                
                let elementDAO = ElementDAO()
                elementDAO.assignElements(elementsToAppend: elementsArray)
                
                completion(elementsArray,error)
            }
        }
    }
    
    func getDataImage(URLImage:String,completion:@escaping ((Data?) -> ())) -> ()
    {
        let imageDAO = DownloadImageDAO()
        
        let dataImage = imageDAO.getDataImage(URLImage: URLImage)
        
        guard dataImage == nil else
        {
            completion(dataImage!)
            return
        }
        
        HTTPClient.sharedInstance.getDataImage(URLImage: URLImage) { (dataImageResult:Data?, error:Error?) in
            
            if dataImageResult != nil
            {
                imageDAO.addImage(URLImage: URLImage, dataImage: dataImageResult!)
            }
            completion(dataImageResult)
        }
    }
    
}
