//
//  DownloadImageView.swift
//  ShowRedditProyect
//
//  Created by User on 3/24/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class DownloadImageView: UIImageView
{
    private let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib()
    {
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        
        loadingIndicator.hidesWhenStopped = true
        
        self.addSubview(loadingIndicator)
        
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    func showDefaultImage()
    {
        self.image = UIImage(named: "default_placeholder")
        loadingIndicator.stopAnimating()
    }
    
    func showImage(imageToShow:UIImage?)
    {
        loadingIndicator.stopAnimating()
        if imageToShow == nil
        {
            self.image = UIImage(named: "default_placeholder")
        }
        else
        {
            self.image = imageToShow
        }
    }
    
    func loadImage(URLImage:String?)
    {
        self.image = UIImage(named: "default_placeholder")
        
        guard URLImage != nil  else
        {
            loadingIndicator.stopAnimating()
            return
        }
        
        loadingIndicator.startAnimating()
        
        LibraryAPI.sharedInstance.getDataImage(URLImage: URLImage!) { (data:Data?) in
        
            if (data != nil)
            {
                DispatchQueue.main.async
                    {
                        self.loadingIndicator.stopAnimating()
                        self.image = UIImage(data: data!)
                }
                
            }
            
        }
        
    }
}
