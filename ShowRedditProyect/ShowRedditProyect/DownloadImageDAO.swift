//
//  DownloadImageDAO.swift
//  ShowRedditProyect
//
//  Created by User on 3/25/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class DownloadImageDAO: AnyObject
{
    func md5(_ string: String) -> String
    {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        
        return hexString
    }
    
    func addImage(URLImage:String,dataImage:Data)
    {
        let fileName = md5(URLImage)
        
        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let URLDocuments = URL(fileURLWithPath: documents)
        let writePathURL = URLDocuments.appendingPathComponent(fileName)
        
        do
        {
            try dataImage.write(to: writePathURL)
        }
        catch
        {
        }
    }
    
    func getDataImage(URLImage:String) -> Data?
    {
        let fileName = md5(URLImage)
        
        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = documents.appending("/" + fileName)
        
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: filePath)
        {
            do
            {
                let dataImage = try Data(contentsOf: URL(fileURLWithPath: filePath))
                
                return dataImage
            }
            catch
            {
                return nil
            }
            
        }
        
        return nil
    }
    
}
