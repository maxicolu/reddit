//
//  RedditTableViewController.swift
//  ShowRedditProyect
//
//  Created by User on 3/23/17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class RedditTableViewController: BaseTableViewController {

    var elementsToShow:Array<Element>?
    
    func reloadElements()
    {
        LibraryAPI.sharedInstance.getElements { (elements:Array<Element>?, error:Error?) in
            self.elementsToShow = elements
            
            DispatchQueue.main.async
            {
                    self.hideLoadingView()
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
            }
            
            
        }
    }
    
    func addRefreshControl()
    {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(reloadElements), for: UIControlEvents.valueChanged)
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        NotificationCenter.default.addObserver(self, selector: #selector(RedditTableViewController.orientationChanged(notification:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        self.navigationItem.title = "Coluccia Maximiliano"
        self.showLoadingView()
        self.addRefreshControl()
        self.reloadElements()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func orientationChanged(notification:NSNotification)
    {
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if elementsToShow != nil && (elementsToShow?.count)! > 0
        {
            self.tableView.separatorStyle = .singleLine
            self.tableView.backgroundView = nil
            
            return 1
        }
        else
        {
            self.tableView.separatorStyle = .none
            
            let noElementsLabel = UILabel()
            noElementsLabel.backgroundColor = UIColor.white
            noElementsLabel.textColor = UIColor.black
            noElementsLabel.text = "No data is currently available. Please pull down to refresh."
            noElementsLabel.textAlignment = .center
            noElementsLabel.font = UIFont.italicSystemFont(ofSize: 20.0)
            noElementsLabel.numberOfLines = 0
            noElementsLabel.sizeToFit()
            
            self.tableView.backgroundView = noElementsLabel
            
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        if elementsToShow != nil
        {
            return elementsToShow!.count
        }
        
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableCellIdentifier", for: indexPath) as! InfoTableViewCell

        // Configure the cell...
        let element = elementsToShow![indexPath.row]
        
        if element.thumbnailURL.contains("http")
        {
            cell.infoImage.loadImage(URLImage: element.thumbnailURL)
        }
        else
        {
            cell.infoImage.showImage(imageToShow: UIImage(named: element.thumbnailURL))
        }
        
        cell.infoTitle.text = element.title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss"
        //dateFormatter.dateStyle = .full
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        cell.detailLabel_1.text = dateFormatter.string(from: element.dateIn)
        
        cell.detailLabel_2.text = element.author
        cell.detailLabel_3.text = String(element.countComments) + " comments"
        cell.detailLabel_4.text = element.subreddit
        
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
